/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pizzaria;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author augus
 */
public abstract class Pizza {
    private String nomeCliente;
    private int idPizza;
    private String nomePizza;
    private boolean cozida;
    protected String receita = ""; 
    private String mensagem;
    private ArrayList<Ingrediente> ingr = new ArrayList();
    
    public boolean insert(){
        boolean cadast=true;
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null; 
        
        String insertTableSQL = "INSERT INTO pizza(nome, cliente, pronta, receita, id) values(?, ?, ?, ?, ?)";
        String sqlIdentifier = "select pizza_pedido.NEXTVAL from dual";
        try{
            
            PreparedStatement pst = dbConnection.prepareStatement(sqlIdentifier);
            synchronized( this ) {
               ResultSet rs = pst.executeQuery();
               if(rs.next())
                 this.idPizza = Integer.parseInt(String.valueOf(rs.getLong(1)));
            }
            
            ps = dbConnection.prepareStatement(insertTableSQL);
            ps.setString(1, this.nomePizza);
            ps.setString(2, this.nomeCliente);
            ps.setBoolean(3, this.cozida);
            ps.setString(4, this.receita);
            ps.setInt(5, this.idPizza);
            
            ps.executeUpdate();
        }
        catch (SQLException e){
            e.printStackTrace();
            cadast=false;
        }
        return cadast;
    }
    
    public static ArrayList<Pizza> getAll(){
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();

        String selectSQL = "SELECT * FROM pizza";
        ArrayList<Pizza> pizza = new ArrayList<>();
        Statement st;
        
        try {
            st = dbConnection.createStatement();
            ResultSet rs = st.executeQuery(selectSQL);
            while (rs.next()){
                System.out.println(rs.getString("nome"));
                Pizza pz = null;
                if("Calabresa".equals(rs.getString("nome"))){
                pz = new Calabresa();
                pz.setNomePizza(rs.getString("nome"));
                pz.setNomeCliente(rs.getString("cliente"));
                pz.setReceita(rs.getString("receita"));
                pz.setIdPizza(rs.getInt("id"));
                pz.setCozida(rs.getBoolean("pronta"));
                if(rs.getBoolean("pronta"))
                    pz.mensagem = "Pronta";
                else
                    pz.mensagem = "Em preparo";
                pizza.add(pz);
                }
                else if("Milho".equals(rs.getString("nome"))){
                pz = new Milho();
                pz.setNomePizza(rs.getString("nome"));
                pz.setNomeCliente(rs.getString("cliente"));
                pz.setReceita(rs.getString("receita"));
                pz.setIdPizza(rs.getInt("id"));
                pz.setCozida(rs.getBoolean("pronta"));
                if(rs.getBoolean("pronta"))
                    pz.mensagem = "Pronta";
                else
                    pz.mensagem = "Em preparo";
                pizza.add(pz);
                }
                else{
                pz = new Portuguesa();
                pz.setNomePizza(rs.getString("nome"));
                pz.setNomeCliente(rs.getString("cliente"));
                pz.setReceita(rs.getString("receita"));
                pz.setIdPizza(rs.getInt("id"));
                pz.setCozida(rs.getBoolean("pronta"));
                if(rs.getBoolean("pronta"))
                    pz.mensagem = "Pronta";
                else
                    pz.mensagem = "Em preparo";
                pizza.add(pz);
                }
            }
        }
        catch (SQLException e){
            e.printStackTrace();
        }
        return pizza;
    }

    public static ArrayList<Pizza> getNaoProntos(){
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();

        String selectSQL = "SELECT * FROM pizza";
        ArrayList<Pizza> pizza = new ArrayList<>();
        Statement st;
        
        try {
            st = dbConnection.createStatement();
            ResultSet rs = st.executeQuery(selectSQL);
            while (rs.next()){
                if(!rs.getBoolean("pronta")){
                if("Calabresa".equals(rs.getString("nome"))){
                Pizza pz = new Calabresa();
                pz.setNomePizza(rs.getString("nome"));
                pz.setNomeCliente(rs.getString("cliente"));
                pz.setReceita(rs.getString("receita"));
                pz.setIdPizza(rs.getInt("id"));
                pz.setCozida(rs.getBoolean("pronta"));
                pizza.add(pz);
                }
                else if("Milho".equals(rs.getString("nome"))){
                Pizza pz = new Milho();
                pz.setNomePizza(rs.getString("nome"));
                pz.setNomeCliente(rs.getString("cliente"));
                pz.setReceita(rs.getString("receita"));
                pz.setIdPizza(rs.getInt("id"));
                pz.setCozida(rs.getBoolean("pronta"));
                pizza.add(pz);
                }
                else{
                Pizza pz = new Portuguesa();
                pz.setNomePizza(rs.getString("nome"));
                pz.setNomeCliente(rs.getString("cliente"));
                pz.setReceita(rs.getString("receita"));
                pz.setIdPizza(rs.getInt("id"));
                pz.setCozida(rs.getBoolean("pronta"));
                pizza.add(pz);
                }
                }
            }
        }
        catch (SQLException e){
            e.printStackTrace();
        }
        return pizza;
    }
    
    public boolean delete(){
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null; 
        String insertTableSQL = "DELETE FROM pizza WHERE id = ?";
        
        try{
                ps = dbConnection.prepareStatement(insertTableSQL);
                ps.setInt(1, this.idPizza);
                ps.executeUpdate();  
           return true;
        }
        catch(SQLException e){
            e.printStackTrace();
            return false;
        }
        finally{
            c.desconecta();
        }
         
    }
    
    public int getIdPizza() {
        return idPizza;
    }

    public void setIdPizza(int idPizza) {
        this.idPizza = idPizza;
    }

    public boolean isCozida() {
        return cozida;
    }

    public void setCozida(boolean cozida) {
        this.cozida = cozida;
    }

    public ArrayList<Ingrediente> getIngr() {
        return ingr;
    }

    public void setIngr(ArrayList<Ingrediente> ingr) {
        this.ingr = ingr;
    }
    
    public void addIngrediente(Ingrediente i){
        ingr.add(i);
    }

    public String getNomeCliente() {
        return nomeCliente;
    }

     public abstract String getReceita();
    
    public String getNomePizza() {
        return nomePizza;
    }

    public void setNomeCliente(String nomeCliente) {
        this.nomeCliente = nomeCliente;
    }

    public void setNomePizza(String nomePizza) {
        this.nomePizza = nomePizza;
    }

    public void setReceita(String receita) {
        this.receita = receita;
    }
    
    
    public boolean update(){
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null; 
        
        String insertTableSQL = "UPDATE  pizza SET pronta = ?  WHERE id = ?";
        
        try{
            ps = dbConnection.prepareStatement(insertTableSQL);
            ps.setInt(2, this.getIdPizza());
            ps.setBoolean(1, true);
            
            ps.executeUpdate();
            
            return true;
        }
        catch(SQLException e){
            e.printStackTrace();
            return false;
        }
    }

    
    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }
}