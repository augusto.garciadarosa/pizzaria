/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pizzaria;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;

/**
 * FXML Controller class
 *
 * @author augus
 */
public class FXMLAtendenteController implements Initializable {

    
    @FXML
    private TableView<Pizza> tb;
    @FXML
    private TextField tfNom;
    @FXML
    private TableColumn<?, ?> idCom;
    @FXML
    private TableColumn<?, ?> CiCom;
    @FXML
    private TableColumn<?, ?> pzCom;
    @FXML
    private TableColumn<?, ?> ptCom;
    
    private Pizza pz;
    @FXML
    private Button btMi;
    @FXML
    private Button btCal;
    @FXML
    private Button btPort;
    @FXML
    private Label labM;
    
    private void addItems(){
        for (Pizza pz : Pizza.getAll()) {
            tb.getItems().add(pz);
        }
    }
    private void makeColumns(){
        CiCom.setCellValueFactory(new PropertyValueFactory<>("NomeCliente"));
        idCom.setCellValueFactory(new PropertyValueFactory<>("IdPizza"));
        pzCom.setCellValueFactory(new PropertyValueFactory<>("NomePizza"));
        ptCom.setCellValueFactory(new PropertyValueFactory<>("Mensagem"));
    }
    
    @FXML
    public void entregar(){
        if(pz.isCozida()==true){
            pz.delete();
            tb.getItems().remove(pz);
            this.labM.setText("");
        }
        else
            this.labM.setText("A pizza não está pronta!");
    }
    
    @FXML
    private void selecionar(MouseEvent event) {
        this.pz = tb.getSelectionModel().getSelectedItem();
    }
    
    @FXML
    public void pedCab(){
        Pizza cab = new Calabresa();
        cab.setNomeCliente(tfNom.getText());
        cab.insert();
        cab.setMensagem("Em preparo");
        tb.getItems().add(cab);
    }
    @FXML
    public void pedPot(){
        Pizza pot = new Portuguesa();
        pot.setNomeCliente(tfNom.getText());
        pot.insert();
        pot.setMensagem("Em preparo");
        tb.getItems().add(pot);
    }
    @FXML
    public void pedMil(){
        Pizza mil = new Milho();
        mil.setNomeCliente(tfNom.getText());
        mil.insert();
        mil.setMensagem("Em preparo");
        tb.getItems().add(mil);
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        makeColumns();
        addItems();
    }    
    
    @FXML
     public void voltar(){
        Pizzaria.trocaTela("FXMLTelaInicial.fxml");
    }
}
