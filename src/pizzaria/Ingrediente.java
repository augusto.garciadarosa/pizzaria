
package pizzaria;


public class Ingrediente {
    private String nome;
    private String quantidade;
    
    public Ingrediente(String nome, String quantidade){
        this.nome = nome;
        this.quantidade = quantidade;
    }
    
    @Override
    public String toString(){
        return " Ingrediente: "+this.nome+" Quantidade: "+this.quantidade;
    }
}
