/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pizzaria;

import java.util.ArrayList;

/**
 *
 * @author augus
 */
public class Portuguesa extends Pizza{
    Ingrediente tomate = new Ingrediente("tomate","200g");
    Ingrediente ovo = new Ingrediente("ovo","2Uni");
    Ingrediente queijo = new Ingrediente("queijo","200g");
    Ingrediente azeitonas = new Ingrediente("azeitona","12Uni");
    Ingrediente molho = new Ingrediente("molho de tomate","50g");
    Ingrediente oregano = new Ingrediente("oregano","7g");
    
    public Portuguesa(){
        this.setCozida(false);
        this.setNomePizza("Portuguesa"); 
        this.addIngrediente(queijo);
        this.addIngrediente(tomate);
        this.addIngrediente(azeitonas);
        this.addIngrediente(molho);
        this.addIngrediente(oregano);
        this.addIngrediente(ovo);
        ArrayList<Ingrediente> ingre = this.getIngr();
        for (Ingrediente i : ingre){
            this.setReceita(this.receita + i);
        }
    }
    
     @Override
    public String getReceita() {
        return this.receita + "";
    }
}
