package pizzaria;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Pizzaria extends Application {
    
    private static Stage stage;
    
    public static Stage getStage() {
        return stage;
    }
    
    public static void trocaTela(String tela){
        Parent root = null;
        try{
            root = FXMLLoader.load(Pizzaria.class.getResource(tela));
        }catch(Exception e){
            System.out.println("Verificar arquivo FXML");
        }
        Scene scene = new Scene(root);
        
        stage.setScene(scene);
        stage.show();
    }
    
    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("FXMLTelaInicial.fxml"));
        
        Scene scene = new Scene(root);
        Pizzaria.stage = stage;
        stage.setScene(scene);
        stage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
