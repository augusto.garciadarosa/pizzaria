/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pizzaria;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;

/**
 * FXML Controller class
 *
 * @author augus
 */
public class FXMLCozinheiroController implements Initializable {

    @FXML
    private TableView<Pizza> tv;
    @FXML
    private TableColumn<?, ?> nomeCom;
    @FXML
    private TableColumn<?, ?> idCom;
    @FXML
    private TableColumn<?, ?> recCom;

    private Pizza pz;
    
   private void addItems(){
        for (Pizza pz : Pizza.getNaoProntos()) {
            tv.getItems().add(pz);
        }
    }
    private void makeColumns(){
        nomeCom.setCellValueFactory(new PropertyValueFactory<>("NomePizza"));
        idCom.setCellValueFactory(new PropertyValueFactory<>("IdPizza"));
        recCom.setCellValueFactory(new PropertyValueFactory<>("Receita"));
    }
    @FXML
    private void cozinhar(){
        pz.update();
        tv.getItems().remove(pz);
    }
   
    @FXML
    private void selecionar(MouseEvent event) {
        this.pz = tv.getSelectionModel().getSelectedItem();
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        makeColumns();
        addItems();
    }    
    @FXML
    public void voltar(){
        Pizzaria.trocaTela("FXMLTelaInicial.fxml");
    }
}
