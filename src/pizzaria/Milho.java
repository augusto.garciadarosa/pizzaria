/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pizzaria;

import java.util.ArrayList;

/**
 *
 * @author augus
 */
public class Milho extends Pizza {

    Ingrediente milho = new Ingrediente("milho", "250g");
    Ingrediente queijo = new Ingrediente("queijo", "200g");
    Ingrediente azeitonas = new Ingrediente("azeitona", "12Uni");
    Ingrediente molho = new Ingrediente("molho de tomate", "50g");
    Ingrediente oregano = new Ingrediente("oregano", "7g");

    public Milho() {
        this.setCozida(false);
        this.setNomePizza("Milho");
        this.addIngrediente( new Ingrediente("milho", "250g"));
        this.addIngrediente(milho);
        this.addIngrediente(azeitonas);
        this.addIngrediente(molho);
        this.addIngrediente(oregano);
        ArrayList<Ingrediente> ingre = this.getIngr();
        for (Ingrediente i : ingre) {
            this.setReceita(this.receita + i);
        }
    }

    @Override
    public String getReceita() {
        return this.receita + "";
    }
}
