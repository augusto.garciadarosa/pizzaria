package pizzaria;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;

public class FXMLTelaInicialController implements Initializable {

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    } 
    @FXML
    public void cozinheiro(){
        Pizzaria.trocaTela("FXMLCozinheiro.fxml");
    }
    @FXML
    public void atendente(){
        Pizzaria.trocaTela("FXMLAtendente.fxml");
    }
    
}
